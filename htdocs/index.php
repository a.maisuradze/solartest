<?php

require __DIR__ . '/../vendor/autoload.php';

// Load .env config
(Dotenv\Dotenv::create(__DIR__ . '/..'))->load();


echo '<pre>';
(new SolarTest\Application)->run();
