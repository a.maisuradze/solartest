<?php
namespace SolarTest;

use Money\Currencies\ISOCurrencies;
use Money\Currency;
use Money\Formatter\IntlMoneyFormatter;
use Money\Money;

class Application
{

    /**
     * @var \mysqli
     */
    static $db;
    /**
     *  Валюта
     */
    CONST CURRENCY = 'RUB';

    /**
     * @var IntlMoneyFormatter
     */
    private $moneyFormatter;


    protected function getMoneyFormatter(){
        if(!$this->moneyFormatter){
            $currencies = new ISOCurrencies();
            $numberFormatter = new \NumberFormatter('ru_RU', \NumberFormatter::CURRENCY);
            $this->moneyFormatter = new IntlMoneyFormatter($numberFormatter, $currencies);
        }
        return $this->moneyFormatter;
    }

    /**
     * Main
     */
    public function run(){
        self::$db = new \mysqli(getenv('DB_HOST'),getenv('DB_USER'),getenv('DB_PASSWORD'),getenv('DB_NAME'),getenv('DB_PORT'));
        self::$db->query("SET NAMES ".getenv('DB_CHARSET'));

        //Load atm #1
        $atm = Application::$db
            ->query("SELECT * FROM atm WHERE id_atm=1")
            ->fetch_object('SolarTest\ATMModel');


        // Fill ATM
        $pack = [
            1000 => 1,
            500 => 3,
//            5000 => 10,
            100 => 5
        ];
        if($atm->fill($pack))
        //if(0)
        {
            echo "ATM fill complete \n";
        } else {
            echo "ATM fill error \n";
        };

        $amount = new Money(170000,new Currency(self::CURRENCY));

        // Withdrawal ATM
        if($atm->withdrawal($amount)){
            echo "ATM withdrawal complete \n";
        } else {
            echo "ATM withdrawal error Выдача отменена \n";
        };



        echo "\nNotes: ";
        print_r($atm->getNotes());

        echo "\nBalance: ";
        echo $this->getMoneyFormatter()->format($atm->getBalance());

        echo "\nSTAT: ";
        print_r($atm->getStatistics());


    }

}