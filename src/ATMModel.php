<?php
namespace SolarTest;
use Money\Money;
use Money\Currency;
final class ATMModel implements ATM
{
    /**
     * @var string
     */
    private $notes;
    /**
     * @var array [Номинал купюры => Количество]
     */
    private $notesArr = [];

    /**
     * Лимит количества купюр одного номинала
     */
    CONST NOTE_LIMIT = 100;

    /**
     * @var Money
     */
    private $balance;

    /**
     * @var Int Identifier
     */
    private $id_atm;

    /**
     * @var String
     */
    private $address;

    /**
     * @var String Валюта банкомата
     */
    private $currency;


    /**
     * @var Currency Валюта банкомата
     */
    private $currencyObj;


    public function getId(): int
    {
        return $this->id_atm;
    }

    /**
     * @return String
     */
    public function getAddress(): String
    {
        return $this->address;
    }

    private function setCurrency(string &$currency): bool
    {
        $this->currency = $currency;
        $this->currencyObj = new Currency($this->currency);
        return true;
    }

    private function setNotesString(string &$notes): bool
    {
        $this->notes = $notes;
        $this->notesArr = unserialize($notes);
        return true;
    }

    /**
     * Вносим купюры
     * @param array $notesArr
     * @return bool
     */
    private function setNotesArr(array &$notesArr): bool
    {
        krsort($notesArr);
        $this->notesArr = $notesArr;
        $this->notes = serialize($notesArr);
        return true;
    }

    public function getNotesString(): string
    {
        return $this->notes;
    }

    /**
     * Получить валюту как строку
     * @return string
     */
    public function getCurrencyString(): string
    {
        return $this->currencyObj."";
    }


    public function __construct()
    {
        $this->setCurrency($this->currency);
        $this->setNotesString($this->notes);
        $this->setBalance($this->getNotes());
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currencyObj;
    }

    /**
     * Загрузка наличных средств в банкомат
     * При превышении лимита - банкноты не добавляются(отменяется все пополнение)
     * @param array  $notes = [
     *     // Номинал купюры => Количество
     *     100 => 25,
     *     5000 => 10,
     * ];
     * @return bool
     */
    public function fill(array $notes): bool
    {
        //Сумма пополнения банкомата
        $amount = $this->calcBalanceFromNotes($notes);

        //Пополнение 0 или меньше рублей
        if(!$amount->isPositive()) return false;

        //Высчитываем итоговое состояние банкомата
        $newNotes = $this->getNotes();
        foreach ($notes as $value=>$count) {
            if(($newNotes[$value] + $count) > self::NOTE_LIMIT) return false;
            $newNotes[$value]+=$count;
        }

        //Текущие банкноты для ролбэка
        $oldNotes = $this->getNotes();

        //Применяем изменения
        $this->setNotesArr($newNotes);
        if($this->save('up', $amount)){
            $this->setBalance($newNotes);
            return true;
        } else {
            $this->setNotesArr($oldNotes);
        }

        return false ;

    }

    /**
     * Выдача наличных средств. Выдавать запрошенную сумму имеющимися купюрами,
     * отдавая приоритет более крупным банкнотам
     *
     * Отмена выдачи при нехватке средств
     *
     * @param Money $amount
     * @return bool
     */
    public function withdrawal(Money $amount): bool
    {
        //Запрошено 0 или меньше рублей
        if(!$amount->isPositive()) return false;

        //Не достаточно денег в банкомате
        if($amount->greaterThan($this->getBalance())) return false;

        $withdrawalAmount = clone $amount;
        $newNotes = [];
        foreach($this->getNotes() as $note=>$count){
            $newNotes[$note] = $count;
            $subCount = $withdrawalAmount->divide($note*100,Money::ROUND_DOWN)->getAmount();

            //Нужны купюры данного номинала
            if($subCount){
                //Купюры к выдаче
                $wCount = 0;
                if($count>=$subCount){
                    //Купюр хватает
                    $wCount = $subCount;
                } else {
                    //Не хватает купюр
                    $wCount = $count;
                }

                $newNotes[$note] -= $wCount;
                $withdrawalAmount = $withdrawalAmount->subtract(
                    new Money(($note*100*$wCount),$this->getCurrency())
                );
                echo "Подготовка купюр ".$note."x".$wCount."\n";
            }
        }
        //Подготовленной суммы недостаточно
        if(!$withdrawalAmount->isZero()) return false;

        //Текущие банкноты для ролбэка
        $oldNotes = $this->getNotes();

        //Применяем изменения
        $this->setNotesArr($newNotes);
        if($this->save('down', $amount)){
            $this->setBalance($newNotes);
            return true;
        } else {
            $this->setNotesArr($oldNotes);
        }

        return false;
    }


    public function getStatistics(): array
    {
        $stmt = Application::$db->prepare("SELECT * FROM transactions WHERE id_atm = ?");
        $stmt->bind_param("i",$this->id_atm);
        $stmt->execute();
        $result = $stmt->get_result();
        $stat = [];
        while($row = $result->fetch_assoc()){
            $stat[] =
                [
                    'type' => $row['type'],
                    'amount' => new Money($row['amount_value'], new Currency($row['amount_currency'])),
                    'date' => new \DateTime($row['date'])
                    ];
        }
        $stmt->close();
        return $stat;
    }

    /**
     * Пополнение/списание
     *
     * @param $type string up|down
     * @param $amount Money
     * @return bool
     */
    private function save(string $type, Money $amount): bool
    {
        try {
            Application::$db->autocommit(FALSE); //turn on transactions

            $stmtATM = Application::$db->prepare("UPDATE atm SET currency = ?, notes = ? WHERE id_atm = ?");
            $stmtTransaction = Application::$db->prepare("INSERT INTO transactions (id_atm,type,amount_value,amount_currency) VALUES (?,?,?,?)");

            $stmtATM->bind_param("ssi",$this->currency,$this->getNotesString(),$this->id_atm);
            $stmtTransaction->bind_param("isis",$this->id_atm, $type,$amount->getAmount(),$this->getCurrencyString());

            $stmtATM->execute();
            $stmtTransaction->execute();

            $stmtATM->close();
            $stmtTransaction->close();

            Application::$db->autocommit(TRUE); //turn off transactions + commit queued queries
        } catch(Exception $e) {
            Application::$db->rollback(); //remove all queries from queue if error (undo)
            throw $e;
            return false;
        }
        return true;
    }

    private function calcBalanceFromNotes(array $notes): Money
    {
        $balance = new Money(0,$this->getCurrency());
        foreach($notes as $note=>$count){
            if($count <= 0) continue;
            if($note <= 0) continue;
            $amount = new Money($note*100,$this->getCurrency());
            $amount = $amount->multiply($count);
            $balance = $balance->add($amount);
        }
        return $balance;
    }


    private function setBalance(array $notes): bool
    {
        $this->balance = $this->calcBalanceFromNotes($notes);
        return true;
    }

    public function getBalance(): Money
    {
        if(!$this->balance) $this->setBalance($this->getNotes());
        return $this->balance;
    }

    public function getNotes(): array
    {
        if(!$this->notesArr) return [];
        return $this->notesArr;
    }
}