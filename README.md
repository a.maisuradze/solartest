**SolarTest**<br />

**Deploy:**<br />
git clone https://gitlab.com/a.maisuradze/solartest.git<br />
cp example.env .env <br />
docker-compose up --build -d
<br /><br />
**DB:**<br />
docker-compose exec mysql bash<br />
mysql -uroot -p test < /tmp/dump/test.db.sql <br />
pass: root
<br /><br />
**Logs:**<br />
mkdir docker/php/logs<br />
mkdir docker/nginx/logs
<br /><br />
**PS:**
 - Не указан изначально список возможных банкнот. Какой массив передан - таким и пополняется.<br />
 - Не делал в базе баланс банкомата. Баланс считается при загрузке АТМ из базы по банкнотам.<br />